# **Introdução**

A seção de Introdução se refere à fase 01 do ciclo de vida do projeto e
tem por objetivo apresentar, de forma refinada, o problema abordado no
projeto.

# **Detalhamento do problema**

Apresentar o problema

# **Levantamento de normas técnicas relacionadas ao problema**

Caso existam, apresentar normas técnicas diretamente associadas ao
problema abordado pela equipe.

# **Identificação de solução comerciais**

Realizar uma pesquisa de soluções já existentes no mercado. É
interessante realizar uma comparação (benchmarking) entre as soluções já
existente.

**Tabela 1: Equipe**

  --------------------------------------------------------------------------
               **Solução    **Solução                            **Solução
               01**         02**                                 N**
  ------------ ------------ ------------ ------------ ---------- -----------
  **Critério   Avaliação da Avaliação da                         
  01**         solução 01   solução 02                           
               quanto ao    quanto ao                            
               critério 01  critério 01                          

  **Critério                                                     
  02**                                                           

                                                                 

  **Critério                                                     
  N**                                                            
  --------------------------------------------------------------------------

# **Objetivo geral do projeto**

Assegurar o bom andamento de um projeto e desenvolvimento, conforme
diretrizes gerais de qualidade.

# **Objetivo específicos do projeto**

Assegurar o bom andamento de um projeto e desenvolvimento, conforme
diretrizes gerais de qualidade.





